%{
  #include <ctype.h>
  #include <stdio.h>

  #include "codex.h"
  #include "stdlog.h"

  // Gobble all block comments: 
  void gobbleToEndOfComment(void);
  void rip(const char *word); 
  void ignore();
  bool listCandidates = false;
  bool evacuateWhenFull = false;
  bool dontRip = false;
  bool printNewLine = false;
  bool printWhites = false;
bool doNotEliminateStrings = false; 
       bool stringIsDoubleDoubleQuote = false;
%}
%x GOBBLE_TO_SEMICOLON 
%x STRING

White           [\t ]+		   
NewLine         [\r\n]+
Letter          [[:alpha:]]|[$|_]
Digit           [[:digit:]]|[_]
xDigit          [[:xdigit:]]|[_]
Identifier      @?{Letter}({Letter}|{Digit})*

OptionalSign    [-+]?
OptionalPrefix  (0[bBpPxX])? 
OptionalSuffix  [fFlLuU]* 
Numeral         {OptionalSign}{OptionalPrefix}{xDigit}+{OptionalSuffix};
FloatingNumeral {Numeral}[eE]{Numeral}

Operator        [-+*/<>^!|:=%&]
OperatorsChain  {Operator}+

%%

%{
/*
{Identifier} { 
  fprintf(stdlog(), "found identifier %s of length %ld", yytext,yyleng); 
  REJECT; 
}
{Letter} { 
  fprintf(stdlog(), "found letter %s of length %ld", yytext,yyleng); 
  REJECT; 
}
{Digit} { 
  fprintf(stdlog(), "found digit %s of length %ld", yytext,yyleng); 
  REJECT; 
}
*/
%}

{White}     if (printWhites)  ECHO; else ignore();     /* Gobble white spaces */
{NewLine}   if (printNewLine) ECHO; else ignore();     /* Gobble white spaces */
[[:cntrl:]] ignore();                             /* Gobble control characters */
"//"{Operator}*.*$             ignore();               /* Gobble line comments */ 
"/*"{Operator}*"*/"{Operator}* ignore();               /* Gobble block comments */ 
"/*"{Operator}*	               gobbleToEndOfComment(); /* Gobble block comments */ 

      /* Gobble all package and import statements */ 
"import"|"package"       BEGIN(GOBBLE_TO_SEMICOLON);
<GOBBLE_TO_SEMICOLON>;   BEGIN(INITIAL);    /* Stop gobbling only at semicolon (;) */ 

<GOBBLE_TO_SEMICOLON>"//"{Operator}*.*$             ignore(); /* line comments */ 
<GOBBLE_TO_SEMICOLON>"/*"{Operator}*"*/"{Operator}* ignore(); /* block comments */ 
<GOBBLE_TO_SEMICOLON>"/*"{Operator}*	              gobbleToEndOfComment(); /* Gobble block comments */ 
<GOBBLE_TO_SEMICOLON><<EOF>>  BEGIN(INITIAL);
<GOBBLE_TO_SEMICOLON>.          ignore();  /* Gobble everything else */  

      /* Gobble contents of string literals */ 
["]                       BEGIN(STRING); if (!listCandidates) { ECHO; if (stringIsDoubleDoubleQuote) ECHO; }
<STRING>[\\]["]           if (doNotEliminateStrings) ECHO; ignore(); 
<STRING>["\n\r\v\f]       if (doNotEliminateStrings) ECHO; BEGIN(INITIAL); 
<STRING><<EOF>>           BEGIN(INITIAL);
<STRING>.                 if (doNotEliminateStrings) ECHO; ignore(); 

.                         if (!listCandidates) ECHO; 
{Identifier}      | 
{Numeral}         |                 
{OperatorsChain}  |                     
{FloatingNumeral}   rip(yytext); 

<<EOF>>           { return 0; }
%%

void ignoreCharacter(const char c) {
  if (isalnum(c) || ispunct(c) || isgraph(c) || c == ' ')
    fprintf(stdlog(), "Ignoring character '%c'\n", c);
  else
    fprintf(stdlog(), "Ignoring character '%x'\n", c);
}

void ignore(void) {
  if (yyleng == 1)
    ignoreCharacter(*yytext);
  else 
    fprintf(stdlog(), "Ignoring '%s' (%ld)\n", yytext, yyleng);
}

void gobbleToEndOfComment(void) {
  int c, prev = 0;
  while ((c = yyinput()) != EOF) {
    if (c == '/' && prev == '*')
      return ;
    prev = c;
  }
}

void rip(const char *word) {
  if (listCandidates) {
    fwrite(yytext,yyleng,1,yyout); 
    fputc('\n',yyout);
    return;
  }
  if (dontRip) {
    fwrite(yytext,yyleng,1,yyout); 
    return;
  }
  int encoding = encode(word);
  if (valid(encoding)) {
    fprintf(stdlog(), "Found '%s', putting %d \n", word, encoding);
    fputc((unsigned char) encoding, yyout);
    return;
  }
  static bool full = false;
might_be_full:
  if (full) {
    if (!evacuateWhenFull) {
      fprintf(stdlog(), "Output '%s' as is (table full)\n", word);
      fwrite(yytext,yyleng,1,yyout); 
      return;
    }
    encoding = evacuateEldest(word);
    fprintf(stdlog(), "Evacuating encoding %d to make place for word '%s' (table full)\n", encoding, word);
    fputc(0, yyout);
    fputc(0, yyout);
    fwrite(yytext,yyleng,1,yyout); 
    fputc(0, yyout);
    return;
  }
  fprintf(stdlog(), "Encoding found for '%s' is %d which is invalid\n", word, encoding);
  encoding = insert(word);
  fprintf(stdlog(), "New encoding (by insert) to '%s' is %d \n", word, encoding);
  if (invalid(encoding)) {
    fprintf(stdlog(), "Encoding %d is invalid. Table must be full!n\n", encoding);
    full = true;
    goto might_be_full; 
  }
  fprintf(stdlog(), "Putting new entry for %s,%d \n", word, encoding);
  fputc(0, yyout);
  fwrite(yytext, yyleng, 1, yyout); 
  fputc(0, yyout);
}
