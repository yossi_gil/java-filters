#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "codex.h"
#include "stdlog.h"

Codex::RankFunctionType Codex::rankFunction = &Codex::rank;

extern bool listCandidates;
extern bool printTableOfEncoding; 
bool prefillWithKeywords = true;

extern bool valid(int encoding) {
  return 1 <= encoding && encoding <= 255;
}

extern bool invalid(int encoding) {
  return !valid(encoding);
}

extern int theInvalidEncoding() {
  return 0;
}

int nextAvailableEncoding() {
  static bool used[256];
  static bool initialized = false;
  if (!initialized && (initialized = true)) 
    for (register int i = ' ' + 1; i < 0x7F; ++i)
      used[i] = true;
  static int $ = 1;
  do {
    if (!used[$] && (used[$] = true))
      return $++;
  } while (valid(++$));
  return theInvalidEncoding();
}

/*
 * Dictionary records are not ordered. Must do a linear search from start to
 * just before first_free.
 */

static int first_free = 0;
static Codex dictionary[256] = {};

int Codex::relative_value() const {
  int $ = 0;
  for (register int i = 0; i < first_free; ++i) 
    $ += value() < dictionary[i].value();
  return $;
}

int Codex::relative_age() const {
  int $ = 0;
  for (register int i = 0; i < first_free; ++i) 
    $ += age() < dictionary[i].age();
  return $;
}

int Codex::relative_length() const {
  int $ = 0;
  for (register int i = 0; i < first_free; ++i) 
    $ += length() < dictionary[i].length();
  return $;
}

int Codex::relative_count() const {
  int $ = 0;
  for (register int i = 0; i < first_free; ++i) 
    $ += count() < dictionary[i].count();
  return $;
}

int Codex::relative_rank() const {
  for (register int $ = 0; $ < first_free; ++$) 
    if (encoding == dictionary[$].encoding) 
        return $;
  return first_free + 1;
}

int totalCount() {
  int $ = 0;
  for (register int i = 0; i < first_free; ++i) 
    $ += dictionary[i].count();
  return $;
}

int totalValue() {
  int $ = 0;
  for (register int i = 0; i < first_free; ++i)
    $ += dictionary[i].value();
  return $;
}

const char * const Codex::format = 
        "%3d %3d "
        "%8d/%-3d "
        "%3d/%-3d " 
        "%4.1f%% (%4.1f%%) "
        "%3d/%-3d "  
        "%8d/%-3d " 
        "%4.1f%% (%4.1f%%) "
        "%4d "
        "'%s'\n";



int Codex::dump(int i, double & partialCount, int totalCount, double& partialValue, int totalValue) const {
  return fprintf(stderr, format, 
      i, encoding, 
      age(), relative_age(),
      count(), relative_count(),
      100. * count() / totalCount, partialCount += 100. * count() / totalCount,
      length(), relative_length(),
      value(), relative_value(),
      100. * value() / totalValue, partialValue += 100. * value() / totalValue,
      rank(),
      decoding
    );
}

void dumpDictionary() {
  if (!printTableOfEncoding)
    return;
  double partialCount =0;
  double partialValue=0;
  for (register int i = 0; i < first_free; ++i) 
    dictionary[i].dump(i, partialCount , totalCount(), partialValue, totalValue());
  fprintf(stderr, "Total count = %d, Total value = %d\n", totalCount(), totalValue());
}

static void swap(int i, int j) {
  Codex::swap(dictionary[i], dictionary[j]);
}

static void sort(int i) {
  for (int j = i; j > 0; --j) 
    if (dictionary[j].value() > dictionary[j-1].value()) {
      swap(j, j -1);
      break;
    }
}

static const Codex touch(int i) {
  fprintf(stdlog(), "Touch %d:\n" , i);
  const Codex $(dictionary[i].touch());
  fprintf(stdlog(), "After Touch %d:\n" , i);
  sort(i);
  fprintf(stdlog(), "After Sort %d:\n" , i);
  return $;
}

const char *decode(int encoding) {
  if (valid(encoding))
    for (register int i = 0; i < first_free; ++i)
      if (encoding == dictionary[i].encoding)
        return touch(i).decoding;
  return (const char *)0;
}

static void prefill();

int encode(const char *decoding) {
  static bool initialized = false;
  if (prefillWithKeywords && !initialized && (initialized = true)) 
    prefill();
  if (decoding != 0) 
    for (register int i = 0; i < first_free; ++i)
      if (strcmp(decoding, dictionary[i].decoding) == 0)
        return touch(i).encoding;
  return theInvalidEncoding(); 
}


int insert(const char *decoding) {
  const int encoding = nextAvailableEncoding();
  if (valid(encoding)) {
    dictionary[first_free].decoding = strdup(decoding);
    dictionary[first_free].encoding = encoding;
    dictionary[first_free].init();
    ++first_free;
    sort(first_free-1);
  }
  return encoding;
}


void prefill() {
  const char *keywords[] = {
    "abstract",
    "assert",
    "boolean",
    "break",
    "byte",
    "case",
    "catch",
    "char",
    "class",
    "const",
    "continue",
    "default",
    "do",
    "double",
    "else",
    "enum",
    "extends",
    "final",
    "finally",
    "float",
    "for",
    "goto",
    "if",
    "implements",
    "import",
    "instanceof",
    "int",
    "interface",
    "long",
    "native",
    "new",
    "package",
    "private",
    "protected",
    "public",
    "return",
    "short",
    "static",
    "strictfp",
    "super",
    "switch",
    "synchronized",
    "this",
    "throw",
    "throws",
    "transient",
    "try",
    "void",
    "volatile",
    "while",
#if 0
    "ArrayList",
    "assertEquals",
    "assertFalse",
    "assertNotNull",
    "assertTrue",
    "hashCode",
    "Integer",
    "length",
    "List",
    "Object",
    "@Override",
    "size",
    "String",
    "@SuppressWarning",
    "@Test",
    "toString",
#endif
    (char*)0,
    };
  for (int i = 0; keywords[i] != 0; ++i)
    insert(keywords[i]);
}

static int findByCurrentRank() {
  int $ = 0; 
  for (register int i = 0; i < first_free; ++i) 
    if ((dictionary[i].*Codex::rankFunction)() < (dictionary[$].*Codex::rankFunction)())
      $ = i;
  return $;
}

void sort() {
  int n = first_free;
  do { 
    int newn = 0;
    for (int i = 1; i < n; ++i) {
      if (dictionary[i].value() > dictionary[i-1].value()) {
        swap(i, i-1);
        newn = i;
      }
    }
    n = newn;
  } while (n > 0);
}

int evacuateEldest(const char *decoding) {
  sort();
  int i = findByCurrentRank();
  fprintf(stdlog(), "Evacuating entry for '%s' to entry %d (decoding is %s)\n", decoding, i, dictionary[i].decoding);
  //dumpDictionary();
  int $ = dictionary[i].reset(decoding);
  assert(i >= 0);
  assert(i < first_free);
  swap(i, first_free - 1);
  sort(first_free - 1);
  fprintf(stdlog(), "After evacuating '%d' for '%s' returning %d\n", i, decoding, $);
  //dumpDictionary();
  return $;
}
