#ifndef _CODEX_H_
#define _CODEX_H_

#include "assert.h"
#include "stdlog.h"

// This codex shall be forgiving, accommodating, culturally tolerant and
// flexible. It is not indented to be bullet proof, but is should be fairly
// robust.  
class Codex { 
  public:

    const char *decoding;
    unsigned char encoding;
    int age() const { return _age; }
    int count() const { return _count; }
    int value() const { return count() * length(); }
    int length() const { return strlen(decoding); }
    int rank() const { 
      return relative_value() + relative_age();
    }
    int relative_age() const;
    int relative_count() const;
    int relative_length() const;
    int relative_value() const;
    int relative_rank() const; 
    Codex(): _age(0), _count(0) {}
    Codex(const Codex &o): 
      decoding(o.decoding), 
      encoding(o.encoding), 
      _age(o.age()), 
      _count(o.count()) 
    {}
    void init() {
      resetAge();
      _count = 0;
    }
    void resetAge() {
      static int clock = 0;
      _age = --clock;
      fprintf(stdlog(), "Age of '%s' set to %d\n", decoding, age());
    }
    const Codex& touch() {
      resetAge();
      ++_count;
    }
    static void swap(Codex &c1, Codex &c2) {
      Codex c1Copy(c1);
      c1 = c2;
      c2 = c1Copy;
    }
    int reset(const char *decoding) {
      assert(decoding != 0);
      fprintf(stdlog(), "Resetting entry '%s' for '%s'\n" , this->decoding, decoding);
      // free(const_cast<char * const>(this->decoding));
      this->decoding = strdup(decoding);
      fprintf(stdlog(), "Old age was %d\n" , this->age());
      init();
      fprintf(stdlog(), "New age is %d\n" , this->age());
      init();
      fprintf(stdlog(), "Resetting for %s; age is %d \n" , decoding, age());
      return this->encoding;
    }
    typedef int (Codex::*RankFunctionType)() const;
    static Codex::RankFunctionType rankFunction;
  private:
    int dump(int i, double & partialCount, int totalCount, double& partialValue, int totalValue) const; 
    static const char * const format; 
    friend void dumpDictionary(void); 
    int _age;
    int _count;
}; 
extern void dumpDictionary(void); 

extern bool prefillWithKeywords;


// An encoding is positive byte, that is an integer in the range 1 through 255.
// An encoding can be represented as an unsigned char. But, not all unsigned
// char values are valid encodings. The encoding 0 is invalid and is never
// used. 
extern bool valid(int encoding); 
extern bool invalid(int encoding); 

// Returns the first invalid encoding 
extern int theInvalidEncoding(); 

extern int evacuateEldest(const char *decoding);

// This function returns an integer which is the next available encoding that
// can be associated with a decoding. A decoding is a sequence of characters,
// typically identifiers, but also numeric literals, sequence of operators, and
// string contents. 
//
// If the returned value is an invalid encoding, then the caller must conclude
// that all available encodings were exhausted.
int nextAvailableEncoding(); 

const char *decode(int encoding); 
int encode(const char *decoding);
int insert(const char *decoding); 

#endif // _CODEX_H_
