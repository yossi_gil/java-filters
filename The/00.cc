#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "codex.h"
#include "scan.h"
#include "stdlog.h"

extern bool listCandidates;
extern bool evacuateWhenFull; 
extern bool dontRip; 
extern bool printWhites; 
extern bool printNewLine; 
extern bool doNotEliminateStrings; 
extern bool stringIsDoubleDoubleQuote; 

bool printTableOfEncoding = false; 

static const char * const *fileNames;
static int errors = 0;

FILE *nextFile() {
  FILE *$;
  for (; *fileNames != 0; ++fileNames) 
    if (($ = fopen(*fileNames, "rb")) == (FILE *)0) 
      ++errors, perror(*fileNames);
    else
      return ++fileNames, $;
  return 0;
}

const char * const *skipFlags(const char * const *$) {
  const char *name = *$;
  for (;;) {
    if (*++$ == (const char *)0)
      return $;
    if (**$ != '-')
      return $;
    switch (1[*$]) {
      case 'h':
        fprintf(stderr, "Usage: %s [flags] [files]\n\n", name);
        fprintf(stderr, "Will process the Java code found in 'files', according to 'flags' as described below to remove . With the absence of the 'files' parameter, '%s' will\n", name); 
        fprintf(stderr, "boilerplate code, comments and the such. \n\n");
        fprintf(stderr, "With the absence of the optional 'files' parameter, '%s' works\n", name); 
        fprintf(stderr, "as a filter, processing whatever found in the standard input.\n\n");
        fprintf(stderr, "Processing includes the following: \n");
        fprintf(stderr, "  1. Filter out all white space characters.\n");
        fprintf(stderr, "  2. Filter out all newline characters.\n");
        fprintf(stderr, "  3. Filter out all control characters.\n");
        fprintf(stderr, "  4. Filter out all remaining non-ASCII characters.\n");
        fprintf(stderr, "  5. Filter out all Java comments\n");
        fprintf(stderr, "  6. Replace all Java strings with one double quote character\n");
        fprintf(stderr, "  7. Filter out all 'import'/'package' statements\n");
        fprintf(stderr, "  8. Encode by a unique non-ASCII byte all Java 'large tokens',\n");
        fprintf(stderr, "     i.e., keywords, and tokens such as '++' and '>>>=' which\n");
        fprintf(stderr, "     contain more than one character.\n");
        fprintf(stderr, "  9. While there are free 1 byte, non-ASCII encodings, use\n");
        fprintf(stderr, "     these to encode more identifiers. \n\n");
        fprintf(stderr, "Flags are:\n\n"); 
        fprintf(stderr, "-h   Help. Print out this message to standard error and terminate.\n"); 
        fprintf(stderr, "-l   List. List all large tokens in the order they occur.\n"); 
        fprintf(stderr, "-s   Silent. Do not produce any output.\n");
        fprintf(stderr, "-d   Dump. Do not replace large tokens, send plain ASCII instead.\n");
        fprintf(stderr, "-w   Do not filter space characters.\n"); 
        fprintf(stderr, "-n   Do not filter new lines.\n"); 
        fprintf(stderr, "-q   Quoted string literals. Output string literals as is, instead of replacing\n)");
        fprintf(stderr, "     these with one double quote character.\n");
        fprintf(stderr, "-Q   double QUOTES. Replace each string literals with two double quote characters.\n");
        fprintf(stderr, "     (this option cancels -q).\n");
        fprintf(stderr, "-z   Zero mode. Equivalent to -d -Q -w -n.\n"); 
        fprintf(stderr, "-t   Table of encoding. Print to the standard error stream the table of encoding\n)");
        fprintf(stderr, "     upon termination.\n");
        fprintf(stderr, "-e   Evacuate when full. Add entries when translation table is full, .\n"); 
        fprintf(stderr, "     keep adding entries.\n"); 
        fprintf(stderr, "-a   Age based evacuation policy. Implies -e.\n");
        fprintf(stderr, "-c   Count based evacuation policy. Implies -e.\n");
        fprintf(stderr, "-v   Value based evacuation policy. Value is count times length(token). Implies -e.\n");
        fprintf(stderr, "-r   Rank based evacuation policy. Rank is  ordinal rank + ordinal age. Implies -e.\n");
        fprintf(stderr, "-p   Do not pre-fill the encoding table with keywords and other large tokens.\n"); 
        exit(0);
        return 0;
      case 'l': 
       listCandidates = true;
       continue;
      case 's': 
       yyout = stdlog();
       continue;
      case 'd':
       dontRip = true; 
       continue;
      case 'w':
       printWhites = true;
       continue;
      case 'n':
       printNewLine = true;
       continue;
      case 'q':
       doNotEliminateStrings = true;
       continue;
      case 'Q':
       doNotEliminateStrings = false;
       stringIsDoubleDoubleQuote = true;
       continue;
      case 'z':
       printWhites = printNewLine = stringIsDoubleDoubleQuote = dontRip = true;
       doNotEliminateStrings = false;
       continue;
      case 't':
        printTableOfEncoding = true; 
        continue;
      case 'e':
        evacuateWhenFull = true;
        continue;
      case 'a': 
       evacuateWhenFull = true; 
       Codex::rankFunction = &Codex::age; 
       continue;
      case 'c': 
       evacuateWhenFull = true; 
       Codex::rankFunction = &Codex::count;
       continue;
      case 'r': 
       evacuateWhenFull = true; 
       Codex::rankFunction = &Codex::rank;
       continue;
      case 'v': 
       evacuateWhenFull = true; 
       Codex::rankFunction = &Codex::value;
       continue;
      case 'p':
       prefillWithKeywords = false;
       continue;
    }
  }
}

int main(int argc, const char * const *argv) {
  locale_t myLocale = newlocale(LC_NUMERIC_MASK, "en_US", (locale_t) 0);
  uselocale(myLocale);
  fileNames = skipFlags(argv);
  if (*fileNames)
    if ((yyin = nextFile()) == (FILE *)0)
      return errors;
  yylex();
  if (printTableOfEncoding)
    dumpDictionary();
  return errors;
}

int yywrap()
{
  yyin == (FILE *)0 || fclose(yyin);
  return ((yyin = nextFile()) == (FILE *)0);
}
