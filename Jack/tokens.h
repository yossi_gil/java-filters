/*
 * tokens.h
 *
 *  Created on: May 24, 2015
 *      Author: Yossi Gil
 */

#ifndef TOKENS_H_
#define TOKENS_H_

enum Token: unsigned char {
  END_OF_FILE                    =     0,
  BEGINNING_OF_FILE              =     1,
  EQUAL_TO                       =     2,    //    ==
  NOT_EQUAL_TO                   =     3,    //!=
  GREATHER_THAN_OR_EQUAL_TO      =     4,    //    >=
  LESS_THAN_OR_EQUAL_TO          =     5,    //    <=
  AMPERSAND_AMPERSAND            =     6,    //    &&
  PIPELINE_PIPELINE              =     7,    //    ||
  PLUS_PLUS                      =     8,    //    ++
  MINUS_MINUS                    =     9,    //    --
  LESS_LESS                      =     10,   //    <<
  GREATER_GREATER                =     11,   //    >>
  GREATER_GREATER_GREATER        =     12,   //
  PLUS_EQUAL                     =     13,   //    +=
  MINUS_EQUAL                    =     14,   //    -=
  ASETRISK_EQUAL                 =     15,   //    *=
  SLASH_EQUAL                    =     16,   //    /=
  PERCENT_EQUAL                  =     17,   //    *=
  AMPERSAND_EQUAL                =     18,   //    &=
  CARET_EQUAL                    =     19,   //    ^=
  PIPELINE_EQUAL                 =     20,   //    |=
  LESS_LESS_EQUAL                =     21,   //    <<=
  GREATER_GREATER_EQUAL          =     22,   //    >>=
  GREATER_GREATER_GREATER_EQUAL  =     23,   //    >>>=
  DIAMOND                        =     24,   //    <> 
  ARROW                          =     25,   //    -> 
  ELIPSIS                        =     26,   //    ...  

  // One character tokens
  CLOSE_BRACKETS    =  ']',
  OPEN_BRACKETS     =  '[',
  CLOSE_PARETHESIS  =  '}',
  OPEN_PARENTHESIS  =  '(',
  CLOSE_BRACES      =  '}',
  OPEN_BRACES       =  '{',
  SEMI_COLON        =  ';',
  COMMA             =  ',',
  PERIOD            =  '.',
  PIPELINE          =  '|',
  CARET             =  '^',
  AMPERSAND         =  '&',
  TILDE             =  '~',
  COLON             =  ':',
  QUESTION_MARK     =  '?',
  EQUAL             =  '=',
  LESS_THAN         =  '<',
  GREATER_THAN      =  '>',
  PERCENT           =  '%',
  SLASH             =  '/',
  ASTERISK          =  '*',
  MINUS             =  '-',
  PLUS              =  '+',
  EXCLAMATION_MARK  =  '!',
  AT_SIGN           =  '@',

// VIM: +,/^\s*$/-!awk '{print $1}' | sort -u | awk '{if(i==0)i=130;$2="=";$3=++i",";print}'|column -t|sed 's/^/  /'
  __             =  131,
  _abstract      =  132,
  _assert        =  133,
  _boolean       =  134,
  _break         =  135,
  _byte          =  136,
  _case          =  137,
  _catch         =  138,
  _char          =  139,
  _class         =  140,
  _const         =  141,
  _continue      =  142,
  _default       =  143,
  _do            =  144,
  _double        =  145,
  _else          =  146,
  _enum          =  147,
  _extends       =  148,
  _false         =  149,
  _final         =  150,
  _finally       =  151,
  _float         =  152,
  _for           =  153,
  _goto          =  154,
  _if            =  155,
  _implements    =  156,
  _import        =  157,
  _instanceof    =  158,
  _int           =  159,
  _interface     =  160,
  _long          =  161,
  _native        =  162,
  _new           =  163,
  _null          =  164,
  _package       =  165,
  _private       =  166,
  _protected     =  167,
  _public        =  168,
  _return        =  169,
  _short         =  170,
  _static        =  171,
  _strictfp      =  172,
  _super         =  173,
  _switch        =  174,
  _synchronized  =  175,
  _this          =  176,
  _throw         =  177,
  _throws        =  178,
  _transient     =  179,
  _true          =  180,
  _try           =  181,
  _void          =  182,
  _volatile      =  183,
  _while         =  184,

  };

extern const char *nameOf(int token); 

#define YY_DECL enum Token tokenizer(void) 
extern YY_DECL;

#endif /* TOKENS_H_ */
