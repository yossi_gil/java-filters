#include "tokens.h"

#include <assert.h>
#include <stdio.h>

#include <cstddef>
#include <type_traits>

template <typename A>
typename std::enable_if <std::is_array <A> ::value, size_t> ::type
SizeOfArray( const A& a )
{
  return std::extent <A> ::value;
}


static const char *token2name[256] = { 
  "END_OF_FILE",
  "BEGINNING_OF_FILE",
	"EQUAL_TO",
	"NOT_EQUAL_TO",
	"GREATER_THAN_OR_EQUAL_TO",
	"LESS_THAN_OR_EQUAL_TO",
	"AMPERSAND_AMPERSAND",
	"PIPELINE_PIPELINE",
	"PLUS_PLUS",
	"MINUS_MINUS",
	"LESS_LESS",
	"GREATER_GREATER",
	"GREATER_GREATER_GREATER",
	"PLUS_EQUAL",
	"MINUS_EQUAL",
	"ASETRISK_EQUAL",
	"SLASH_EQUAL",
	"PERCENT_EQUAL",
	"AMPERSAND_EQUAL",
	"CARET_EQUAL",
	"PIPELINE_EQUAL",
	"LESS_LESS_EQUAL",
	"GREATER_GREATER_EQUAL",
	"GREATER_GREATER_GREATER_EQUAL",
	"ARROW",
	"DIAMOND",
};

static void initialize();

extern const char *nameOf(int t) {
  assert(t >= 0);
  assert(t < SizeOfArray(token2name));
  initialize();
  return token2name[t];
}

static void initialize() {
  static bool done = false;
  if (done || !(done = true))
    return;
  for (int i = 0; i < SizeOfArray(token2name); ++i)
    if (token2name[i] == 0) {
      char *leak = new char[10];
      sprintf(leak, "'%c'", i);
      token2name[i] = leak;
    }
  token2name[_abstract] = "abstract";
  token2name[_assert] = "assert";
  token2name[ _boolean] = "boolean";
  token2name[ _break] = "break";
  token2name[ _byte] = "byte";
  token2name[ _case] = "case";
  token2name[ _catch] = "catch";
  token2name[ _char] = "char";
  token2name[ _class] = "class";
  token2name[_const] = "const";
  token2name[_continue] = "continue";
  token2name[_default] = "default";
  token2name[_do] = "do";
  token2name[_double] = "double";
  token2name[_else] = "else";
  token2name[ _enum] = "enum";
  token2name[ _extends] = "extends";
  token2name[ _final] = "final";
  token2name[ _finally] = "finally";
  token2name[ _float] = "float";
  token2name[ _for] = "for";
  token2name[ _goto] = "goto";
  token2name[ _if] = "if";
  token2name[ _implements] = "implements";
  token2name[ _import] = "import";
  token2name[ _instanceof] = "instanceof";
  token2name[ _int] = "int";
  token2name[ _interface] = "interface";
  token2name[ _long] = "long";
  token2name[ _native] = "native";
  token2name[ _new] = "new";
  token2name[ _package] = "package";
  token2name[ _private] = "private";
  token2name[ _protected] = "protected";
  token2name[ _public] = "public";
  token2name[ _return] = "return";
  token2name[ _short] = "short";
  token2name[ _static] = "static";
  token2name[ _strictfp] = "strictfp";
  token2name[ _super] = "super";
  token2name[ _switch] = "switch";
  token2name[ _synchronized] = "synchronized";
  token2name[ _this] = "this";
  token2name[ _throw] = "throw";
  token2name[ _throws] = "throws" ;
  token2name[ _transient] = "transient";
  token2name[ _try] = "try";
  token2name[ _void] = "void";
  token2name[ _volatile] = "volatile";
  token2name[ _while] = "while";
}
