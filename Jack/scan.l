%{
  #include <stdio.h>
  #include "tokens.h"
  // Gobble all block comments: 
  void comment();
  #define RETURN  return static_cast<enum Token>(*yytext)
%}
%x GOBBLE 
%x STRING
%x CHAR
%x QCHAR
WS [ \t\v\n\f\r\v]+		   

%%
      /* Gobble all white space: */ 
{WS}

      /* Gobble all comments */ 
"//"[.]*$             /* line comments */ 
"/*"			               comment(); 

      /* Gobble all package and import statements */ 
"import"                  | 
"package"                 BEGIN(GOBBLE);
<GOBBLE>;                 BEGIN(INITIAL);    /* Stop gobbling only at semicolon (;) */ 
<GOBBLE>"/*"			        |
<GOBBLE>"*/"			        comment(); 
<GOBBLE>"//"[.]*$         /* gobble line comments */ 
<GOBBLE>.                 /* Gobble everything else */  

      /* Gobble contents of string literals */ 
["]                       BEGIN(STRING); RETURN;
<STRING>[\\]["]
<STRING>[\n\r\v\f"]       BEGIN(INITIAL); 
<STRING>.                  

      /* Deal with character literals */ 
"'"                       BEGIN(CHAR); RETURN;
<CHAR>[\n\r\v\f']         BEGIN(INITIAL); RETURN;
<CHAR>"\\"                BEGIN(QCHAR); RETURN;
<QCHAR>.                  BEGIN(CHAR); RETURN;
<CHAR>.                   RETURN;

  /* Keywords in alphabetical order *
  // VIM: /"/,/^\s*$/-! awk '{$3="";print}' | sort -u | awk '{$3= "_"substr($1,2,length($1)-2);$4=";";print}'|column -t
  */             
  "_"             return   __ ;
  "abstract"      return  _abstract      ;
  "assert"        return  _assert        ;
  "boolean"       return  _boolean       ;
  "break"         return  _break         ;
  "byte"          return  _byte          ;
  "case"          return  _case          ;
  "catch"         return  _catch         ;
  "char"          return  _char          ;
  "class"         return  _class         ;
  "const"         return  _const         ;
  "continue"      return  _continue      ;
  "default"       return  _default       ;
  "do"            return  _do            ;
  "double"        return  _double        ;
  "else"          return  _else          ;
  "enum"          return  _enum          ;
  "extends"       return  _extends       ;
  "false"         return  _false         ;
  "finally"       return  _finally       ;
  "final"         return  _final         ;
  "float"         return  _float         ;
  "for"           return  _for           ;
  "goto"          return  _goto          ;
  "if"            return  _if            ;
  "implements"    return  _implements    ;
  "instanceof"    return  _instanceof    ;
  "interface"     return  _interface     ;
  "int"           return  _int           ;
  "long"          return  _long          ;
  "native"        return  _native        ;
  "new"           return  _new           ;
  "null"          return  _null          ;
  "private"       return  _private       ;
  "protected"     return  _protected     ;
  "public"        return  _public        ;
  "return"        return  _return        ;
  "short"         return  _short         ;
  "static"        return  _static        ;
  "strictfp"      return  _strictfp      ;
  "super"         return  _super         ;
  "switch"        return  _switch        ;
  "synchronized"  return  _synchronized  ;
  "this"          return  _this          ;
  "throw"         return  _throw         ;
  "throws"        return  _throws        ;
  "transient"     return  _transient     ;
  "true"          return  _true          ;
  "try"           return  _try           ;
  "void"          return  _void          ;
  "volatile"      return  _volatile      ;
  "while"         return  _while         ;

  /*  Operators of length 4: */
">>>="  return  GREATER_GREATER_GREATER_EQUAL;

  /*  Operators of length 3 
  // VIM: /"/,/^\s*$/-! sort -u | column -t
   */
">>="  return  GREATER_GREATER_EQUAL ;
"..."  return  ELIPSIS                ;
">>>"  return  GREATER_GREATER_GREATER ;
"<<="  return  LESS_LESS_EQUAL ;

  /* Operators of length 2: 
  // VIM: /"/,/^\s*$/-! sort -u | column -t
   */
"&&"  return  AMPERSAND_AMPERSAND        ;
"&="  return  AMPERSAND_EQUAL            ;
"->"  return  ARROW                      ;
"*="  return  ASETRISK_EQUAL             ;
"^="  return  CARET_EQUAL                ;
"<>"  return  DIAMOND                    ;
"=="  return  EQUAL_TO                   ;
">>"  return  GREATER_GREATER            ;
">="  return  GREATHER_THAN_OR_EQUAL_TO  ;
"<<"  return  LESS_LESS                  ;
"<="  return  LESS_THAN_OR_EQUAL_TO      ;
"-="  return  MINUS_EQUAL                ;
"--"  return  MINUS_MINUS                ;
"!="  return  NOT_EQUAL_TO               ;
"%="  return  PERCENT_EQUAL              ;
"|="  return  PIPELINE_EQUAL             ;
"||"  return  PIPELINE_PIPELINE          ;
"+="  return  PLUS_EQUAL                 ;
"++"  return  PLUS_PLUS                  ;
"/="  return  SLASH_EQUAL                ;

  /* One character operators: */
.       RETURN;

  /* EOF: */
  <<EOF>>   return END_OF_FILE;
%%

void comment(void) {
	char c, prev = 0;
	while ((c = yyinput()) != 0) {
    /* (EOF maps to 0) */
		if (c == '/' && prev == '*')
			return ;
		prev = c;
	}
}
