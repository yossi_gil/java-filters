#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokens.h"
#include "scan.h"

static const char * const *fileNames;
static int errors = 0;

FILE *nextFile() {
  FILE *$;
  for (; *fileNames != 0; ++fileNames) {
    if (($ = fopen(*fileNames, "r")) == (FILE *)0) 
      ++errors, perror(*fileNames);
    else
      return ++fileNames, $;
  }
  return 0;
}

int main(int argc, const char * const *argv) {
  fileNames = argv + 1;
  if (*fileNames)
    if ((yyin = nextFile()) == (FILE *)0)
      return errors;
  int c;
  while ((c = tokenizer()) != 0)
    fputc(c, stdout);
  return errors;
}

int yywrap()
{
  fputc(END_OF_FILE, stdout);
  yyin == (FILE *)0 || fclose(yyin);
  return ((yyin = nextFile()) == (FILE *)0);
}
