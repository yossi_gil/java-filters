module Main where

import System.Environment

x |> f = f x
infixl 1 |>
(.>) = flip (.)
x <&> f = fmap f x
infixl 1 <&>

isPrefix :: (Eq a) => [a] -> [a] -> Bool
isPrefix [] (y:ys) = True
isPrefix (x:xs) (y:ys) = if x == y then isPrefix xs ys else False
isPrefix _ _ = False

filterNot :: (a -> Bool) -> [a] -> [a]
filterNot = filter . not' where
  not' = (.) not


removeStringLiterals :: String -> String
removeStringLiterals = tail [] where
  tail res [] = reverse res
  tail res ('"':xs) = waitForCloser ('"':res) xs
  tail res (x:xs) = tail (x:res) xs
  waitForCloser res ('\\':'"':xs) = waitForCloser res xs
  waitForCloser res ('"':xs) = tail ('"':res) xs
  waitForCloser res (_:xs) = waitForCloser res xs
  waitForCloser res [] = tail res []

removeLineComments :: String -> String
removeLineComments = tail [] .> reverse where
  tail res [] = res
  tail res ('/':'/':_) = res
  tail res (x:xs) = tail (x:res) xs

removeBlockComments :: [String] -> [String]
removeBlockComments = unlines .> tail [] .> lines where
  tail res [] = reverse res
  tail res ('/':'*':xs) = tail res $ waitForCloser xs []
  tail res (x:xs) = tail (x:res) xs
  waitForCloser [] res = res
  waitForCloser ('\n':xs) res = waitForCloser xs $ '\n':res
  waitForCloser ('*':'/':xs) res = res ++ xs
  waitForCloser (_:xs) res = waitForCloser xs res

isImport :: String -> Bool
isImport = isPrefix "import"

isPackage :: String -> Bool
isPackage = isPrefix "package"

isEmpty :: [a] -> Bool
isEmpty [] = True
isEmpty _ = False

currify :: String -> String
currify = lines
  .> map removeStringLiterals
  .> removeBlockComments
  .> map removeLineComments
  .> filterNot isImport
  .> filterNot isPackage
  .> filterNot isEmpty
  .> unlines

main = do getArgs <&> head >>= readFile <&> currify >>= putStrLn

